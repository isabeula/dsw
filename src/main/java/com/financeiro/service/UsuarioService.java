package com.financeiro.service;

import java.util.List;

import com.financeiro.model.Usuario;

public interface UsuarioService {
	
	Usuario incluirUsuario(Usuario user);
	Usuario alterarUsuario(Usuario user);
	Usuario deletarUsuario(Usuario user);
	Usuario buscarUsuarioPorId(Integer id);
	List<Usuario> listarUsuarios();
	Usuario buscarUserAtivoPeloEmail(String email);
	Usuario findUsuarioActiveByEmail(String email);

}
