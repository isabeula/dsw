package com.financeiro.service;

import java.util.List;

import com.financeiro.model.Permissao;

public interface PermissaoService {
	
	Permissao incluirPermissao(Permissao permissao);
	Permissao alterarPermissao(Permissao permissao);
	Permissao deletarPermissao(Permissao permissao);
	Permissao buscarPermissaoPorId(Integer id);
	List<Permissao> listarPermissoes();
}
