package com.financeiro.service;

import java.util.List;

import com.financeiro.model.RolePermissao;
import com.financeiro.model.RolePermissaoId;

public interface RolePermissaoService {
	
	RolePermissao incluirRolePermissao(RolePermissao rp);
	RolePermissao alterarRolePermissao(RolePermissao rp);
	void deletarRolePermissao(RolePermissao rp);
	RolePermissao buscarRolePermissaoPorId(RolePermissaoId rp);
	List<RolePermissao> listarRolePermissoes();
}
