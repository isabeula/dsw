package com.financeiro.service;

import java.util.List;

import com.financeiro.model.Caixa;

public interface CaixaService {
	
	Caixa incluirCaixa(Caixa caixa);
	Caixa alterarCaixa(Caixa caixa);
	Caixa deletarCaixa(Caixa caixa);
	Caixa buscarCaixaPorId(Integer id);
	List<Caixa> listarCaixas();

}
