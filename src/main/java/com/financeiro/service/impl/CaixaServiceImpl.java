package com.financeiro.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.financeiro.model.Caixa;
import com.financeiro.repository.CaixaRepository;
import com.financeiro.service.CaixaService;

@Service
@Transactional
public class CaixaServiceImpl implements CaixaService {

	@Autowired
	private CaixaRepository caixaRepository;
	
	@Override
	public Caixa incluirCaixa(Caixa caixa) {
        return caixaRepository.saveAndFlush(caixa);
	}

	@Override
	public Caixa alterarCaixa(Caixa caixa) {
		return caixaRepository.saveAndFlush(caixa);
	}

	@Override
	public Caixa deletarCaixa(Caixa caixa) {
		Caixa caixaEncontrada = buscarCaixaPorId(caixa.getId());
		caixaRepository.delete(caixaEncontrada);
		return null;
	}

	@Override
	@Transactional(readOnly=true)
	public Caixa buscarCaixaPorId(Integer id) {
		return caixaRepository.getOne(id);
	}

	@Override
	@Transactional(readOnly=true)
	public List<Caixa> listarCaixas() {
		return caixaRepository.findAll();
	}

}
