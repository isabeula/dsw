package com.financeiro.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.financeiro.model.Telefone;
import com.financeiro.repository.TelefoneRepository;
import com.financeiro.service.TelefoneService;

@Service
@Transactional
public class TelefoneServiceImpl implements TelefoneService {

	@Autowired
	private TelefoneRepository telefoneRepository;
	
	@Override
	public Telefone incluirTelefone(Telefone telefone) {
        return telefoneRepository.saveAndFlush(telefone);
	}

	@Override
	public Telefone alterarTelefone(Telefone telefone) {
		return telefoneRepository.saveAndFlush(telefone);
	}

	@Override
	public Telefone deletarTelefone(Telefone telefone) {
		Telefone telefoneEncontrada = buscarTelefonePorId(telefone.getId());
		telefoneRepository.delete(telefoneEncontrada);
		return null;
	}

	@Override
	@Transactional(readOnly=true)
	public Telefone buscarTelefonePorId(Integer id) {
		return telefoneRepository.getOne(id);
	}

	@Override
	@Transactional(readOnly=true)
	public List<Telefone> listarTelefones() {
		return telefoneRepository.findAll();
	}

}
