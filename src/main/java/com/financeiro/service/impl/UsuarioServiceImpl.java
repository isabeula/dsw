package com.financeiro.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.financeiro.model.Usuario;
import com.financeiro.repository.UsuarioRepository;
import com.financeiro.service.UsuarioService;

@Service
@Transactional
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	
	@Override
	public Usuario incluirUsuario(Usuario usuario) {
        return usuarioRepository.saveAndFlush(usuario);
	}

	@Override
	public Usuario alterarUsuario(Usuario usuario) {
		return usuarioRepository.saveAndFlush(usuario);
	}

	@Override
	public Usuario deletarUsuario(Usuario usuario) {
		Usuario usuarioEncontrada = buscarUsuarioPorId(usuario.getId());
		usuarioRepository.delete(usuarioEncontrada);
		return null;
	}

	@Override
	@Transactional(readOnly=true)
	public Usuario buscarUsuarioPorId(Integer id) {
		return usuarioRepository.getOne(id);
	}

	@Override
	@Transactional(readOnly=true)
	public List<Usuario> listarUsuarios() {
		return usuarioRepository.findAll();
	}
	
	@Override
	public Usuario buscarUserAtivoPeloEmail(String email) {
		return usuarioRepository.buscarUserAtivoPeloEmail(email);
		
	}

	@Override
	public Usuario findUsuarioActiveByEmail(String email) {
		return usuarioRepository.buscarUserAtivoPeloEmail(email);
	}

}
