package com.financeiro.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.financeiro.model.Pessoa;
import com.financeiro.repository.PessoaRepository;
import com.financeiro.service.PessoaService;

@Service
@Transactional
public class PessoaServiceImpl implements PessoaService {

	@Autowired
	private PessoaRepository pessoaRepository;
	
	@Override
	public Pessoa incluirPessoa(Pessoa pessoa) {
        return pessoaRepository.saveAndFlush(pessoa);
	}

	@Override
	public Pessoa alterarPessoa(Pessoa pessoa) {
		return pessoaRepository.saveAndFlush(pessoa);
	}

	@Override
	public Pessoa deletarPessoa(Pessoa pessoa) {
		Pessoa pessoaEncontrada = buscarPessoaPorId(pessoa.getId());
		pessoaRepository.delete(pessoaEncontrada);
		return null;
	}

	@Override
	@Transactional(readOnly=true)
	public Pessoa buscarPessoaPorId(Integer id) {
		return pessoaRepository.getOne(id);
	}

	@Override
	@Transactional(readOnly=true)
	public List<Pessoa> listarPessoas() {
		return pessoaRepository.findAll();
	}

}
