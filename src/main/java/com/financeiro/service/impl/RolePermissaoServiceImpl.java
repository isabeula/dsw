package com.financeiro.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.financeiro.model.RolePermissao;
import com.financeiro.model.RolePermissaoId;
import com.financeiro.repository.RolePermissaoRepository;
import com.financeiro.service.RolePermissaoService;

@Service
@Transactional
public class RolePermissaoServiceImpl implements RolePermissaoService {

	@Autowired
	private RolePermissaoRepository rpRepository;
	
	@Override
	public RolePermissao incluirRolePermissao(RolePermissao rp) {
        return rpRepository.saveAndFlush(rp);
	}

	@Override
	public RolePermissao alterarRolePermissao(RolePermissao rp) {
		return rpRepository.saveAndFlush(rp);
	}

	@Override
	public void deletarRolePermissao(RolePermissao rp) {
		RolePermissao rpEncontrada = buscarRolePermissaoPorId(rp.getId());
		rpRepository.delete(rpEncontrada);
	}

	@Override
	@Transactional(readOnly=true)
	public RolePermissao buscarRolePermissaoPorId(RolePermissaoId rp) {
		return rpRepository.getOne(rp);
	}

	@Override
	@Transactional(readOnly=true)
	public List<RolePermissao> listarRolePermissoes() {
		return rpRepository.findAll();
	}

}
