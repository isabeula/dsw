package com.financeiro.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.financeiro.model.Permissao;
import com.financeiro.repository.PermissaoRepository;
import com.financeiro.service.PermissaoService;

@Service
@Transactional
public class PermissaoServiceImpl implements PermissaoService {

	@Autowired
	private PermissaoRepository permissaoRepository;
	
	@Override
	public Permissao incluirPermissao(Permissao permissao) {
        return permissaoRepository.saveAndFlush(permissao);
	}

	@Override
	public Permissao alterarPermissao(Permissao permissao) {
		return permissaoRepository.saveAndFlush(permissao);
	}

	@Override
	public Permissao deletarPermissao(Permissao permissao) {
		Permissao permissaoEncontrada = buscarPermissaoPorId(permissao.getId());
		permissaoRepository.delete(permissaoEncontrada);
		return null;
	}

	@Override
	@Transactional(readOnly=true)
	public Permissao buscarPermissaoPorId(Integer id) {
		return permissaoRepository.getOne(id);
	}

	@Override
	@Transactional(readOnly=true)
	public List<Permissao> listarPermissoes() {
		return permissaoRepository.findAll();
	}

}
