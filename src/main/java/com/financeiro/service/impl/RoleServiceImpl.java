package com.financeiro.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.financeiro.model.Role;
import com.financeiro.repository.RoleRepository;
import com.financeiro.service.RoleService;

@Service
@Transactional
public class RoleServiceImpl implements RoleService {

	@Autowired
	private RoleRepository cargoRepository;
	
	@Override
	public Role incluirRole(Role cargo) {
        return cargoRepository.saveAndFlush(cargo);
	}

	@Override
	public Role alterarRole(Role cargo) {
		return cargoRepository.saveAndFlush(cargo);
	}

	@Override
	public Role deletarRole(Role cargo) {
		Role cargoEncontrada = buscarRolePorId(cargo.getId());
		cargoRepository.delete(cargoEncontrada);
		return null;
	}

	@Override
	@Transactional(readOnly=true)
	public Role buscarRolePorId(Integer id) {
		return cargoRepository.getOne(id);
	}

	@Override
	@Transactional(readOnly=true)
	public List<Role> listarRoles() {
		return cargoRepository.findAll();
	}

}
