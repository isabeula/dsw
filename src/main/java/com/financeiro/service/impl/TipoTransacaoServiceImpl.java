package com.financeiro.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.financeiro.model.TipoTransacao;
import com.financeiro.repository.TipoTransacaoRepository;
import com.financeiro.service.TipoTransacaoService;

@Service
@Transactional
public class TipoTransacaoServiceImpl implements TipoTransacaoService {

	@Autowired
	private TipoTransacaoRepository tipoTransacaoRepository;
	
	@Override
	public TipoTransacao incluirTipoTransacao(TipoTransacao tipoTransacao) {
        return tipoTransacaoRepository.saveAndFlush(tipoTransacao);
	}

	@Override
	public TipoTransacao alterarTipoTransacao(TipoTransacao tipoTransacao) {
		return tipoTransacaoRepository.saveAndFlush(tipoTransacao);
	}

	@Override
	public TipoTransacao deletarTipoTransacao(TipoTransacao tipoTransacao) {
		TipoTransacao tipoTransacaoEncontrada = buscarTipoTransacaoPorId(tipoTransacao.getId());
		tipoTransacaoRepository.delete(tipoTransacaoEncontrada);
		return null;
	}

	@Override
	@Transactional(readOnly=true)
	public TipoTransacao buscarTipoTransacaoPorId(Integer id) {
		return tipoTransacaoRepository.getOne(id);
	}

	@Override
	@Transactional(readOnly=true)
	public List<TipoTransacao> listarTipoTransacoes() {
		return tipoTransacaoRepository.findAll();
	}

}
