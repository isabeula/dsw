package com.financeiro.service;

import java.util.List;

import com.financeiro.model.Role;

public interface RoleService {
	
	Role incluirRole(Role cargo);
	Role alterarRole(Role cargo);
	Role deletarRole(Role cargo);
	Role buscarRolePorId(Integer id);
	List<Role> listarRoles();
}
