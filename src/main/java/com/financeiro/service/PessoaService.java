package com.financeiro.service;

import java.util.List;

import com.financeiro.model.Pessoa;

public interface PessoaService {
	
	Pessoa incluirPessoa(Pessoa pessoa);
	Pessoa alterarPessoa(Pessoa pessoa);
	Pessoa deletarPessoa(Pessoa pessoa);
	Pessoa buscarPessoaPorId(Integer id);
	List<Pessoa> listarPessoas();

}
