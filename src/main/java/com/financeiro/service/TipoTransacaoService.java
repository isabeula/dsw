package com.financeiro.service;

import java.util.List;

import com.financeiro.model.TipoTransacao;

public interface TipoTransacaoService {
	
	TipoTransacao incluirTipoTransacao(TipoTransacao tipoTransacao);
	TipoTransacao alterarTipoTransacao(TipoTransacao tipoTransacao);
	TipoTransacao deletarTipoTransacao(TipoTransacao tipoTransacao);
	TipoTransacao buscarTipoTransacaoPorId(Integer id);
	List<TipoTransacao> listarTipoTransacoes();

}
