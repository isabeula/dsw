package com.financeiro.service;

import java.util.List;

import com.financeiro.model.Telefone;

public interface TelefoneService {
	
	Telefone incluirTelefone(Telefone tel);
	Telefone alterarTelefone(Telefone tel);
	Telefone deletarTelefone(Telefone tel);
	Telefone buscarTelefonePorId(Integer id);
	List<Telefone> listarTelefones();

}
