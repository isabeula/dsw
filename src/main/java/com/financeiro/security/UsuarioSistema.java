package com.financeiro.security;

import org.springframework.security.core.userdetails.User;

import com.financeiro.model.Usuario;
import com.financeiro.service.UsuarioService;

public class UsuarioSistema extends User {
		
	/**
	 * 
	 */
	private static final long serialVersionUID = -5157735194055582506L;
	private Usuario usuario;
	
	public UsuarioSistema(Usuario user) {
		super(user.getEmail(),
				user.getPassword(),
				user.isAccountNonExpired(),
				user.isAccountNonLocked(),
				user.isCredentialsNonExpired(),
				user.isEnabled(),
				user.getAuthorities());
		this.usuario = user;
	}	
	
	public Usuario getUsuario() {
		return usuario;
	}
}
