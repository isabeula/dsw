package com.financeiro.security;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.financeiro.model.Usuario;
import com.financeiro.service.UsuarioService;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UsuarioService usuarioService;
	
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		
		Usuario user = buscarUsuarioSistema(email);
		if(Objects.isNull(user)) {
			throw new UsernameNotFoundException("Usuário não encontrado" + email);
		}
		return new UsuarioSistema(user);
	}

	private Usuario buscarUsuarioSistema(String email) {
		return usuarioService.findUsuarioActiveByEmail(email);
	}

}
