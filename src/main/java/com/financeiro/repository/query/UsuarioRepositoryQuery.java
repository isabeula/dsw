package com.financeiro.repository.query;

import com.financeiro.model.Usuario;

public interface UsuarioRepositoryQuery {
	Usuario buscarUserAtivoPeloEmail(String email);
	Usuario findUsuarioActiveByEmail(String email);
}
