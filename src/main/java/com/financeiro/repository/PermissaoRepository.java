package com.financeiro.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.financeiro.model.Permissao;
import com.financeiro.model.RolePermissaoId;

@Repository
public interface PermissaoRepository extends JpaRepository<Permissao, Integer> {

}

