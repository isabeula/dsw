package com.financeiro.repository.impl;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.financeiro.model.Usuario;
import com.financeiro.repository.query.UsuarioRepositoryQuery;

public class UsuarioRepositoryImpl implements UsuarioRepositoryQuery {
	
	@PersistenceContext
	private EntityManager entityManager;
		
	@Override
	public Usuario buscarUserAtivoPeloEmail(String email) {	
		
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Usuario> cq = cb.createQuery(Usuario.class);
		Root<Usuario> rootFromUsuario = cq.from(Usuario.class);

		Predicate predicates = cb.conjunction();
		
		predicates = cb.and(cb.equal(rootFromUsuario.get("email"), email), cb.equal(rootFromUsuario.get("ativo"), true));
		cq.where(predicates);
		cq.select(rootFromUsuario);
		
		TypedQuery<Usuario> query = entityManager.createQuery(cq);
	
			Usuario user = query.getSingleResult();
			return user;
		
	}
	
	//IZI WAY -> mesma coisa do buscarUserAtivoPeloEmail, porém mais "facil"
	public Usuario findUsuarioActiveByEmail(String email) {
		TypedQuery<Usuario> query = entityManager.createQuery("SELECT u FROM Usuario u lower(u.email) = lower(:email)"
																+ "u.ativo = true", Usuario.class);
		query.setParameter("email", email);
		Usuario user = query.getSingleResult();
		return user;
	}
	

}
