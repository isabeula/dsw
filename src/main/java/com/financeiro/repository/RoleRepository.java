package com.financeiro.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.financeiro.model.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {

}

