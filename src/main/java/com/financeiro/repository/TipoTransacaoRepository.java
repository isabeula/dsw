package com.financeiro.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.financeiro.model.TipoTransacao;

@Repository
public interface TipoTransacaoRepository extends JpaRepository<TipoTransacao, Integer> {

}
