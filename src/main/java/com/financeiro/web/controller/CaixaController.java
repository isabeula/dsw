package com.financeiro.web.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.financeiro.model.TipoTransacao;
import com.financeiro.model.Caixa;
import com.financeiro.service.TipoTransacaoService;
import com.financeiro.service.CaixaService;

@Controller
@RequestMapping(value="/caixa")
public class CaixaController {

	@Autowired
	private CaixaService caixaService;
	
	@Autowired
	private TipoTransacaoService tipoTransacaoService;
	
	
	@RequestMapping(value="/cadastrar")
	public ModelAndView cadastrarCaixa(Caixa caixa) {
		ModelAndView mv = new ModelAndView("/caixa/cadastrar");
		mv.addObject("caixa",caixa);
		return mv;
	}
	
	@RequestMapping(value="/listar")
	public String listarCaixa(ModelMap model) {
		model.addAttribute("listaCaixas", caixaService.listarCaixas());
		return "/caixa/listar";
	}
		
	@RequestMapping(value="/salvar",method=RequestMethod.POST, params="action=salvar")
	public String salvarCaixa(Caixa caixa, BindingResult result, RedirectAttributes attr) {
		if (result.hasErrors()) {
			return "/caixa/cadastrar";
		}
		caixaService.incluirCaixa(caixa);
		return "redirect:/caixa/listar";
	}
	
	@RequestMapping(value="/editar/{id}", method=RequestMethod.GET)
	public ModelAndView editarCaixa(@PathVariable("id") Integer id) {
		Caixa caixa = caixaService.buscarCaixaPorId(id);
		ModelAndView mv = new ModelAndView("/caixa/cadastrar");
		mv.addObject("caixa", caixa);
		return mv;
	}
	
	@RequestMapping(value="/editar",method=RequestMethod.POST)
	public String editarCaixa(@Valid Caixa caixa, BindingResult result, RedirectAttributes attr) {
		if (result.hasErrors()) {
			return "/caixa/cadastrar";
		}
		caixaService.alterarCaixa(caixa);
		return "redirect:/caixa/cadastrar";
	}
	
	@RequestMapping(value="/excluir/{id}", method=RequestMethod.GET)
	public ModelAndView excluirCaixa(@PathVariable("id") Integer id) {
		Caixa caixa = caixaService.buscarCaixaPorId(id);
		ModelAndView mv = new ModelAndView("/caixa/excluir");
		mv.addObject("caixa", caixa);
		return mv;
	}
	
	@RequestMapping(value="/excluir",method=RequestMethod.POST)
	public String removerCaixa(Caixa caixa, BindingResult result, RedirectAttributes attr) {
		caixaService.deletarCaixa(caixa);
		return "redirect:/caixa/listar";
	}
		
	@RequestMapping(value="/consultar/{id}", method=RequestMethod.GET)
	public ModelAndView consultarCaixa(@PathVariable("id") Integer id) {
		Caixa caixa = caixaService.buscarCaixaPorId(id);
		ModelAndView mv = new ModelAndView("/caixa/consultar");
		mv.addObject("caixa", caixa);
		return mv;
	}
	
	@RequestMapping(value= {"/salvar", "/editar", "/excluir", "/consultar"}, method=RequestMethod.POST, params="action=cancelar")
	public String cancelar() {
		return "redirect:/caixa/pesquisar";
	}
	
	@ModelAttribute("tipoTransacaos")
	public List<TipoTransacao> listarTodasTipoTransacaos(){
		return tipoTransacaoService.listarTipoTransacoes();
	}
	

	public TipoTransacaoService getTipoTransacaoService() {
		return tipoTransacaoService;
	}
}
