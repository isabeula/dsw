package com.financeiro.web.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.financeiro.model.Usuario;
import com.financeiro.service.UsuarioService;

@Controller
@RequestMapping(value="/usuario")
public class UsuarioController {

	@Autowired
	private UsuarioService usuarioService;
	
	
	@RequestMapping(value="/cadastrar")
	public ModelAndView cadastrarUsuario(Usuario usuario) {
		ModelAndView mv = new ModelAndView("/usuario/cadastrar");
		mv.addObject("usuario",usuario);
		return mv;
	}
	
	@RequestMapping(value="/listar")
	public String listarUsuario(ModelMap model) {
		model.addAttribute("listaUsuarios", usuarioService.listarUsuarios());
		return "/usuario/listar";
	}
		
	@RequestMapping(value="salvar",method=RequestMethod.POST)
	public String salvarUsuario(@Valid Usuario usuario, BindingResult result, RedirectAttributes attr) {
		if (result.hasErrors()) {
			return "/usuario/cadastrar";
		}
		usuarioService.incluirUsuario(usuario);
		return "redirect:/usuario/listar";
	}
	
	@RequestMapping(value="/editar/{id}", method=RequestMethod.GET)
	public ModelAndView editarUsuario(@PathVariable("id") Integer id) {
		Usuario usuario = usuarioService.buscarUsuarioPorId(id);
		ModelAndView mv = new ModelAndView("/usuario/cadastrar");
		mv.addObject("usuario", usuario);
		return mv;
	}
	
	@RequestMapping(value="/editar",method=RequestMethod.POST)
	public String editarUsuario(@Valid Usuario usuario, BindingResult result, RedirectAttributes attr) {
		if (result.hasErrors()) {
			return "/usuario/cadastrar";
		}
		usuarioService.alterarUsuario(usuario);
		return "redirect:/usuario/cadastrar";
	}
	
	@RequestMapping(value="/excluir/{id}", method=RequestMethod.GET)
	public ModelAndView excluirUsuario(@PathVariable("id") Integer id) {
		Usuario usuario = usuarioService.buscarUsuarioPorId(id);
		ModelAndView mv = new ModelAndView("/usuario/excluir");
		mv.addObject("usuario", usuario);
		return mv;
	}
	
	@RequestMapping(value="/excluir",method=RequestMethod.POST)
	public String removerUsuario(Usuario usuario, BindingResult result, RedirectAttributes attr) {
		usuarioService.deletarUsuario(usuario);
		return "redirect:/usuario/listar";
	}
		
	@RequestMapping(value="/consultar/{id}", method=RequestMethod.GET)
	public ModelAndView consultarUsuario(@PathVariable("id") Integer id) {
		Usuario usuario = usuarioService.buscarUsuarioPorId(id);
		ModelAndView mv = new ModelAndView("/usuario/consultar");
		mv.addObject("usuario", usuario);
		return mv;
	}
	
	@RequestMapping(value= {"/salvar", "/editar", "/excluir", "/consultar"}, method=RequestMethod.POST, params="action=cancelar")
	public String cancelar() {
		return "redirect:/usuario/pesquisar";
	}
}
