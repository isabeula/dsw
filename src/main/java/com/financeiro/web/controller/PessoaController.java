package com.financeiro.web.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.financeiro.model.Pessoa;
import com.financeiro.model.estatico.Modelo;
import com.financeiro.model.estatico.UF;
import com.financeiro.service.PessoaService;

@Controller
@RequestMapping(value="/pessoa")
public class PessoaController {

	@Autowired
	private PessoaService pessoaService;
	
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}
	
	
	@RequestMapping(value="/cadastrar")
	public ModelAndView cadastrarPessoa(Pessoa pessoa) {
		ModelAndView mv = new ModelAndView("/pessoa/cadastrar");
		mv.addObject("pessoa",pessoa);
		return mv;
	}
	
	@RequestMapping(value="/listar")
	public String listarPessoa(ModelMap model) {
		model.addAttribute("listaPessoas", pessoaService.listarPessoas());
		return "/pessoa/listar";
	}
		
	@RequestMapping(value="salvar",method=RequestMethod.POST)
	public String salvarPessoa(@Valid Pessoa pessoa, BindingResult result, RedirectAttributes attr) {
		if (result.hasErrors()) {
			return "/pessoa/cadastrar";
		}
		pessoaService.incluirPessoa(pessoa);
		return "redirect:/telefone/cadastrar";
	}
	
	@RequestMapping(value="/editar/{id}", method=RequestMethod.GET)
	public ModelAndView editarPessoa(@PathVariable("id") Integer id) {
		Pessoa pessoa = pessoaService.buscarPessoaPorId(id);
		ModelAndView mv = new ModelAndView("/pessoa/cadastrar");
		mv.addObject("pessoa", pessoa);
		return mv;
	}
	
	@RequestMapping(value="/editar",method=RequestMethod.POST)
	public String editarPessoa(@Valid Pessoa pessoa, BindingResult result, RedirectAttributes attr) {
		if (result.hasErrors()) {
			return "/pessoa/cadastrar";
		}
		pessoaService.alterarPessoa(pessoa);
		return "redirect:/pessoa/cadastrar";
	}
	
	@RequestMapping(value="/excluir/{id}", method=RequestMethod.GET)
	public ModelAndView excluirPessoa(@PathVariable("id") Integer id) {
		Pessoa pessoa = pessoaService.buscarPessoaPorId(id);
		ModelAndView mv = new ModelAndView("/pessoa/excluir");
		mv.addObject("pessoa", pessoa);
		return mv;
	}
	
	@RequestMapping(value="/excluir",method=RequestMethod.POST)
	public String removerPessoa(Pessoa pessoa, BindingResult result, RedirectAttributes attr) {
		pessoaService.deletarPessoa(pessoa);
		return "redirect:/pessoa/listar";
	}
		
	@RequestMapping(value="/consultar/{id}", method=RequestMethod.GET)
	public ModelAndView consultarPessoa(@PathVariable("id") Integer id) {
		Pessoa pessoa = pessoaService.buscarPessoaPorId(id);
		ModelAndView mv = new ModelAndView("/pessoa/consultar");
		mv.addObject("pessoa", pessoa);
		return mv;
	}
	
	@RequestMapping(value= {"/salvar", "/editar", "/excluir", "/consultar"}, method=RequestMethod.POST, params="action=cancelar")
	public String cancelar() {
		return "redirect:/pessoa/pesquisar";
	}
	
	@ModelAttribute("estados")
	public UF[] getUnidadeFederal() {
		return UF.values();
	}
	
	
}
