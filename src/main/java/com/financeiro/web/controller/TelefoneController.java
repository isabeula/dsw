package com.financeiro.web.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.financeiro.model.Pessoa;
import com.financeiro.model.Telefone;
import com.financeiro.model.estatico.Modelo;
import com.financeiro.model.estatico.Tipo;
import com.financeiro.service.PessoaService;
import com.financeiro.service.TelefoneService;

@Controller
@RequestMapping(value="/telefone")
public class TelefoneController {

	@Autowired
	private TelefoneService telefoneService;
	
	@Autowired
	private PessoaService pessoaService;
	
	
	@RequestMapping(value="/cadastrar")
	public ModelAndView cadastrarTelefone(Telefone telefone) {
		ModelAndView mv = new ModelAndView("/telefone/cadastrar");
		mv.addObject("telefone",telefone);
		return mv;
	}
	
	@RequestMapping(value="/listar")
	public String listarTelefone(ModelMap model) {
		model.addAttribute("listaTelefones", telefoneService.listarTelefones());
		return "/telefone/listar";
	}
		
	@RequestMapping(value="/salvar",method=RequestMethod.POST, params="action=salvar")
	public String salvarTelefone(Telefone telefone, BindingResult result, RedirectAttributes attr) {
		if (result.hasErrors()) {
			return "/telefone/cadastrar";
		}
		telefoneService.incluirTelefone(telefone);
		return "redirect:/telefone/listar";
	}
	
	@RequestMapping(value="/editar/{id}", method=RequestMethod.GET)
	public ModelAndView editarTelefone(@PathVariable("id") Integer id) {
		Telefone telefone = telefoneService.buscarTelefonePorId(id);
		ModelAndView mv = new ModelAndView("/telefone/cadastrar");
		mv.addObject("telefone", telefone);
		return mv;
	}
	
	@RequestMapping(value="/editar",method=RequestMethod.POST)
	public String editarTelefone(@Valid Telefone telefone, BindingResult result, RedirectAttributes attr) {
		if (result.hasErrors()) {
			return "/telefone/cadastrar";
		}
		telefoneService.alterarTelefone(telefone);
		return "redirect:/telefone/cadastrar";
	}
	
	@RequestMapping(value="/excluir/{id}", method=RequestMethod.GET)
	public ModelAndView excluirTelefone(@PathVariable("id") Integer id) {
		Telefone telefone = telefoneService.buscarTelefonePorId(id);
		ModelAndView mv = new ModelAndView("/telefone/excluir");
		mv.addObject("telefone", telefone);
		return mv;
	}
	
	@RequestMapping(value="/excluir",method=RequestMethod.POST)
	public String removerTelefone(Telefone telefone, BindingResult result, RedirectAttributes attr) {
		telefoneService.deletarTelefone(telefone);
		return "redirect:/telefone/listar";
	}
		
	@RequestMapping(value="/consultar/{id}", method=RequestMethod.GET)
	public ModelAndView consultarTelefone(@PathVariable("id") Integer id) {
		Telefone telefone = telefoneService.buscarTelefonePorId(id);
		ModelAndView mv = new ModelAndView("/telefone/consultar");
		mv.addObject("telefone", telefone);
		return mv;
	}
	
	@RequestMapping(value= {"/salvar", "/editar", "/excluir", "/consultar"}, method=RequestMethod.POST, params="action=cancelar")
	public String cancelar() {
		return "redirect:/telefone/pesquisar";
	}
	
	@ModelAttribute("pessoas")
	public List<Pessoa> listarTodasPessoas(){
		return pessoaService.listarPessoas();
	}
	

	public PessoaService getPessoaService() {
		return pessoaService;
	}
	
	@ModelAttribute("modelos")
	public Modelo[] getModelos() {
		return Modelo.values();
	}
	
	@ModelAttribute("tipos")
	public Tipo[] getTipos() {
		return Tipo.values();
	}
}
