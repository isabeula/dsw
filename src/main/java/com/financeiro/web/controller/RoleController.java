package com.financeiro.web.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.financeiro.model.Role;
import com.financeiro.service.RoleService;

@Controller
@RequestMapping(value="/role")
public class RoleController {

	@Autowired
	private RoleService roleService;
	
	
	@RequestMapping(value="/cadastrar")
	public ModelAndView cadastrarRole(Role role) {
		ModelAndView mv = new ModelAndView("/role/cadastrar");
		mv.addObject("role",role);
		return mv;
	}
	
	@RequestMapping(value="/listar")
	public String listarRole(ModelMap model) {
		model.addAttribute("listaRoles", roleService.listarRoles());
		return "/role/listar";
	}
		
	@RequestMapping(value="salvar",method=RequestMethod.POST)
	public String salvarRole(@Valid Role role, BindingResult result, RedirectAttributes attr) {
		if (result.hasErrors()) {
			return "/role/cadastrar";
		}
		roleService.incluirRole(role);
		return "redirect:/role/listar";
	}
	
	@RequestMapping(value="/editar/{id}", method=RequestMethod.GET)
	public ModelAndView editarRole(@PathVariable("id") Integer id) {
		Role role = roleService.buscarRolePorId(id);
		ModelAndView mv = new ModelAndView("/role/cadastrar");
		mv.addObject("role", role);
		return mv;
	}
	
	@RequestMapping(value="/editar",method=RequestMethod.POST)
	public String editarRole(@Valid Role role, BindingResult result, RedirectAttributes attr) {
		if (result.hasErrors()) {
			return "/role/cadastrar";
		}
		roleService.alterarRole(role);
		return "redirect:/role/cadastrar";
	}
	
	@RequestMapping(value="/excluir/{id}", method=RequestMethod.GET)
	public ModelAndView excluirRole(@PathVariable("id") Integer id) {
		Role role = roleService.buscarRolePorId(id);
		ModelAndView mv = new ModelAndView("/role/excluir");
		mv.addObject("role", role);
		return mv;
	}
	
	@RequestMapping(value="/excluir",method=RequestMethod.POST)
	public String removerRole(Role role, BindingResult result, RedirectAttributes attr) {
		roleService.deletarRole(role);
		return "redirect:/role/listar";
	}
		
	@RequestMapping(value="/consultar/{id}", method=RequestMethod.GET)
	public ModelAndView consultarRole(@PathVariable("id") Integer id) {
		Role role = roleService.buscarRolePorId(id);
		ModelAndView mv = new ModelAndView("/role/consultar");
		mv.addObject("role", role);
		return mv;
	}
	
	@RequestMapping(value= {"/salvar", "/editar", "/excluir", "/consultar"}, method=RequestMethod.POST, params="action=cancelar")
	public String cancelar() {
		return "redirect:/role/listar";
	}
}
