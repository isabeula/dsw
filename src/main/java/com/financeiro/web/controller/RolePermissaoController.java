package com.financeiro.web.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.financeiro.model.Permissao;
import com.financeiro.model.Role;
import com.financeiro.model.RolePermissao;
import com.financeiro.model.RolePermissaoId;
import com.financeiro.service.PermissaoService;
import com.financeiro.service.RolePermissaoService;
import com.financeiro.service.RoleService;

@Controller
@RequestMapping(value="/rolePermissao")
public class RolePermissaoController {
	
	@Autowired
	private PermissaoService permissaoService;
	
	@Autowired
	private RoleService roleService;
	
	@Autowired
	private RolePermissaoService rolePermissaoService;
	
	
	@InitBinder
    protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }
	
	
    //realiza o carregamento da página com o objeto trabalhado
	@RequestMapping(value="/cadastrar", method=RequestMethod.GET)
	public ModelAndView cadastrarRolePermissao(RolePermissao rolePermissao) {
		ModelAndView mv = new ModelAndView("/rolePermissao/cadastrar");
		mv.addObject("rolepermissao", rolePermissao);
		return mv;
	}
	
	@RequestMapping(value="/salvar", method=RequestMethod.POST,params="action=salvar")
	public ModelAndView salvar(RolePermissao rolePermissao, BindingResult result,  RedirectAttributes attr) {
		if (result.hasErrors()) {
			return cadastrarRolePermissao(rolePermissao);
		}
		
		RolePermissaoId rpId = new RolePermissaoId(rolePermissao.getPermissaoId().getId(), 
												   rolePermissao.getRoleId().getId());
		
		rolePermissao.setId(rpId);
		rolePermissaoService.incluirRolePermissao(rolePermissao);
		attr.addFlashAttribute("success", "Registro inserido com sucesso.");
		return new ModelAndView("/direitos/cadastro");
	}
	
	
	
	@ModelAttribute("roles")
	public List<Role> listarTodasRoles(){
		return roleService.listarRoles();
	}
	
	
	@ModelAttribute("permissoes")
	public List<Permissao> listarTodasPermissoes(){
		return permissaoService.listarPermissoes();
	}


	public PermissaoService getPermissaoService() {
		return permissaoService;
	}


	public void setPermissaoService(PermissaoService permissaoService) {
		this.permissaoService = permissaoService;
	}


	public RoleService getRoleService() {
		return roleService;
	}


	public void setRoleService(RoleService roleService) {
		this.roleService = roleService;
	}


	public RolePermissaoService getRolePermissaoService() {
		return rolePermissaoService;
	}


	public void setRolePermissaoService(RolePermissaoService rolePermissaoService) {
		this.rolePermissaoService = rolePermissaoService;
	}
	

}
