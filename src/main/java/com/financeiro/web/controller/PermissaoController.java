package com.financeiro.web.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.financeiro.model.Permissao;
import com.financeiro.service.PermissaoService;

@Controller
@RequestMapping(value="/permissao")
public class PermissaoController {

	@Autowired
	private PermissaoService permissaoService;
	
	
	@RequestMapping(value="/cadastrar")
	public ModelAndView cadastrarPermissao(Permissao permissao) {
		ModelAndView mv = new ModelAndView("/permissao/cadastrar");
		mv.addObject("permissao",permissao);
		return mv;
	}
	
	@RequestMapping(value="/listar")
	public String listarPermissao(ModelMap model) {
		model.addAttribute("listaPermissoes", permissaoService.listarPermissoes());
		return "/permissao/listar";
	}
		
	@RequestMapping(value="salvar",method=RequestMethod.POST)
	public String salvarPermissao(@Valid Permissao permissao, BindingResult result, RedirectAttributes attr) {
		if (result.hasErrors()) {
			return "/permissao/cadastrar";
		}
		permissaoService.incluirPermissao(permissao);
		return "redirect:/permissao/listar";
	}
	
	@RequestMapping(value="/editar/{id}", method=RequestMethod.GET)
	public ModelAndView editarPermissao(@PathVariable("id") Integer id) {
		Permissao permissao = permissaoService.buscarPermissaoPorId(id);
		ModelAndView mv = new ModelAndView("/permissao/cadastrar");
		mv.addObject("permissao", permissao);
		return mv;
	}
	
	@RequestMapping(value="/editar",method=RequestMethod.POST)
	public String editarPermissao(@Valid Permissao permissao, BindingResult result, RedirectAttributes attr) {
		if (result.hasErrors()) {
			return "/permissao/cadastrar";
		}
		permissaoService.alterarPermissao(permissao);
		return "redirect:/permissao/cadastrar";
	}
	
	@RequestMapping(value="/excluir/{id}", method=RequestMethod.GET)
	public ModelAndView excluirPermissao(@PathVariable("id") Integer id) {
		Permissao permissao = permissaoService.buscarPermissaoPorId(id);
		ModelAndView mv = new ModelAndView("/permissao/excluir");
		mv.addObject("permissao", permissao);
		return mv;
	}
	
	@RequestMapping(value="/excluir",method=RequestMethod.POST)
	public String removerPermissao(Permissao permissao, BindingResult result, RedirectAttributes attr) {
		permissaoService.deletarPermissao(permissao);
		return "redirect:/permissao/listar";
	}
		
	@RequestMapping(value="/consultar/{id}", method=RequestMethod.GET)
	public ModelAndView consultarPermissao(@PathVariable("id") Integer id) {
		Permissao permissao = permissaoService.buscarPermissaoPorId(id);
		ModelAndView mv = new ModelAndView("/permissao/consultar");
		mv.addObject("permissao", permissao);
		return mv;
	}
	
	@RequestMapping(value= {"/salvar", "/editar", "/excluir", "/consultar"}, method=RequestMethod.POST, params="action=cancelar")
	public String cancelar() {
		return "redirect:/permissao/listar";
	}
}
