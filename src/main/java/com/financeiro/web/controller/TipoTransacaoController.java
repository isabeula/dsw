package com.financeiro.web.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.financeiro.model.TipoTransacao;
import com.financeiro.service.TipoTransacaoService;

@Controller
@RequestMapping(value="/tipoTransacao")
public class TipoTransacaoController {

	@Autowired
	private TipoTransacaoService tipoTransacaoService;
	
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}
	
	
	@RequestMapping(value="/cadastrar")
	public ModelAndView cadastrarTipoTransacao(TipoTransacao tipoTransacao) {
		ModelAndView mv = new ModelAndView("/tipoTransacao/cadastrar");
		mv.addObject("tipoTransacao",tipoTransacao);
		return mv;
	}
	
	@RequestMapping(value="/listar")
	public String listarTipoTransacao(ModelMap model) {
		model.addAttribute("listaTipoTransacaos", tipoTransacaoService.listarTipoTransacoes());
		return "/tipoTransacao/listar";
	}
		
	@RequestMapping(value="salvar",method=RequestMethod.POST)
	public String salvarTipoTransacao(@Valid TipoTransacao tipoTransacao, BindingResult result, RedirectAttributes attr) {
		if (result.hasErrors()) {
			return "/tipoTransacao/cadastrar";
		}
		tipoTransacaoService.incluirTipoTransacao(tipoTransacao);
		return "redirect:/caixa/cadastrar";
	}
	
	@RequestMapping(value="/editar/{id}", method=RequestMethod.GET)
	public ModelAndView editarTipoTransacao(@PathVariable("id") Integer id) {
		TipoTransacao tipoTransacao = tipoTransacaoService.buscarTipoTransacaoPorId(id);
		ModelAndView mv = new ModelAndView("/tipoTransacao/cadastrar");
		mv.addObject("tipoTransacao", tipoTransacao);
		return mv;
	}
	
	@RequestMapping(value="/editar",method=RequestMethod.POST)
	public String editarTipoTransacao(@Valid TipoTransacao tipoTransacao, BindingResult result, RedirectAttributes attr) {
		if (result.hasErrors()) {
			return "/tipoTransacao/cadastrar";
		}
		tipoTransacaoService.alterarTipoTransacao(tipoTransacao);
		return "redirect:/tipoTransacao/cadastrar";
	}
	
	@RequestMapping(value="/excluir/{id}", method=RequestMethod.GET)
	public ModelAndView excluirTipoTransacao(@PathVariable("id") Integer id) {
		TipoTransacao tipoTransacao = tipoTransacaoService.buscarTipoTransacaoPorId(id);
		ModelAndView mv = new ModelAndView("/tipoTransacao/excluir");
		mv.addObject("tipoTransacao", tipoTransacao);
		return mv;
	}
	
	@RequestMapping(value="/excluir",method=RequestMethod.POST)
	public String removerTipoTransacao(TipoTransacao tipoTransacao, BindingResult result, RedirectAttributes attr) {
		tipoTransacaoService.deletarTipoTransacao(tipoTransacao);
		return "redirect:/tipoTransacao/listar";
	}
		
	@RequestMapping(value="/consultar/{id}", method=RequestMethod.GET)
	public ModelAndView consultarTipoTransacao(@PathVariable("id") Integer id) {
		TipoTransacao tipoTransacao = tipoTransacaoService.buscarTipoTransacaoPorId(id);
		ModelAndView mv = new ModelAndView("/tipoTransacao/consultar");
		mv.addObject("tipoTransacao", tipoTransacao);
		return mv;
	}
	
	@RequestMapping(value= {"/salvar", "/editar", "/excluir", "/consultar"}, method=RequestMethod.POST, params="action=cancelar")
	public String cancelar() {
		return "redirect:/tipoTransacao/pesquisar";
	}
	
	
}
