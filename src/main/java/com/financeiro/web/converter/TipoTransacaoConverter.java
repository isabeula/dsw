package com.financeiro.web.converter;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.financeiro.model.TipoTransacao;
//import com.financeiro.model.security.TipoTransacao;
import com.financeiro.service.TipoTransacaoService;

@Component
public class TipoTransacaoConverter implements Converter<String, TipoTransacao>{
	
	@Autowired
	private TipoTransacaoService ttService;

	@Override
	public TipoTransacao convert(String string) {
        if (string.isEmpty()) {
        	return null;
        }
        Integer id = Integer.valueOf(string);
		return ttService.buscarTipoTransacaoPorId(id);
	}

}