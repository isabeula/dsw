package com.financeiro.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;


@Configuration
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter{
	
	@Autowired
	private UserDetailsService userDetailsService;
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception{
		/*String senhacript = passwordEncoder().encode("1234");
		auth.inMemoryAuthentication()
				.withUser("user")
				.password(senhacript)
				.roles("USUARIO");*/
		
		//String senhacript = passwordEncoder().encode("123456");		
		String senhascript = passwordEncoder().encode("123456");
		
		System.out.print("senha"+senhascript+"senha");
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		http.authorizeRequests()
				//.antMatchers("/**/**").permitAll()
				.antMatchers("/caixa/**").permitAll()
				.antMatchers("/tipoTransacao/**").permitAll()
				.antMatchers("/resources/**","/js/**","/static/**","/css/**","images/**").permitAll()
				.antMatchers("/login","/").permitAll()
				.antMatchers("/pessoa/**").hasAnyRole("ADM","USER")
				.antMatchers("/usuario/**").hasRole("ADM")
				.antMatchers("/role/**").hasRole("ADM")
				.antMatchers("/permissao/**").hasRole("ADM")
				.antMatchers("/rolePermissao/**").hasRole("ADM")
				
				.antMatchers("/login","/").permitAll()
				
				.anyRequest().authenticated()
				.and()
			
			.formLogin()
				.loginPage("/login")
				.usernameParameter("email")
				.passwordParameter("password")
				.defaultSuccessUrl("/home",true)
				.failureUrl("/login")
				.permitAll()
				.and()
				
			.csrf().disable();
	}
	
	@Override
	public void configure(WebSecurity web) {
		web.ignoring().antMatchers("/resources/**");
	}
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
}
