package com.financeiro.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name="TAB_ROLE_PERMISSAO")
public class RolePermissao implements Serializable {

	private static final long serialVersionUID = -1477899201665527979L;
	
	private RolePermissaoId id;
	private Role roleId;
	private Permissao permissaoId;
	private Date dataCadastro;
	
	public RolePermissao() {
	}

	public RolePermissao(RolePermissaoId id, Role roleId, Permissao permissaoId, Date dataCadastro) {
		this.id = id;
		this.roleId = roleId;
		this.permissaoId = permissaoId;
		this.dataCadastro = dataCadastro;
	}
	
	@EmbeddedId
	public RolePermissaoId getId() {
		return id;
	}
	public void setId(RolePermissaoId id) {
		this.id = id;
	}
	
	@ManyToOne
	@JoinColumn(name="role_id",insertable=false, updatable=false, nullable=false)
	public Role getRoleId() {
		return roleId;
	}
	public void setRoleId(Role roleId) {
		this.roleId = roleId;
	}
	
	@ManyToOne
	@JoinColumn(name="permissao_id",insertable=false, updatable=false, nullable=false)
	public Permissao getPermissaoId() {
		return permissaoId;
	}
	public void setPermissaoId(Permissao permissaoId) {
		this.permissaoId = permissaoId;
	}
	
	//@NotNull(message="A data de cadastro deve ser informada.")
	@Temporal(TemporalType.DATE)
	@Column(name="role_permissao_data_cadastro",nullable=false, columnDefinition="DATE")
	public Date getDataCadastro() {
		return dataCadastro;
	}
	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RolePermissao other = (RolePermissao) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
	@Override
	public String toString() {
		return "RolePermissao [id=" + id + ", role_id=" + roleId + ", permissao_id=" + permissaoId + ", dataCadastro="
				+ dataCadastro + "]";
	}
	
	
	
	

}
