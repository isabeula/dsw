package com.financeiro.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

@Entity
@Table(name="TAB_PESSOA")
public class Pessoa implements Serializable {

	private static final long serialVersionUID = 1391651266613023535L;
	
	private Integer id;
	private String nome;
	private Endereco endereco;
	private boolean ativo = true;
	private Date dataNascimento;
	private String foto;
	private String contentType;
	private BigDecimal salario;
	private List<Telefone> listaTel;
	
	public Pessoa() {
		
	}


	public Pessoa(Integer id, String nome, Endereco endereco, boolean ativo, BigDecimal salario,
			Date dataNascimento, String foto, String contentType) {

		this.id = id;
		this.nome = nome;
		this.endereco = endereco;
		this.ativo = ativo;
		this.salario = salario;
		this.dataNascimento = dataNascimento;
		this.foto = foto;
		this.contentType = contentType;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pessoa_id")
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@NotBlank(message="Informe o nome da pessoa. ") 
	@NotNull(message="Informe o nome da pessoa.")
	@Size(min=3, max=100, message="O nome da pessoa deve ter entre {min} e {max} caracteres. ")
	@Column(name="pessoa_nome",length=100, nullable=false)
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Valid
	@Embedded
	public Endereco getEndereco() {
		return endereco;
	}
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	
	@NotNull
	@Column(name="pessoa_ativo", nullable=false)
	public boolean isAtivo() {
		return ativo;
	}
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	
	@NumberFormat(style=Style.CURRENCY, pattern="#,##0.00")
	@Column(name="pessoa_salario", nullable=true, columnDefinition="DECIMAL(10,2) DEFAULT 0.00")
	public BigDecimal getSalario() {
		return salario;
	}


	public void setSalario(BigDecimal salario) {
		this.salario = salario;
	}


	@DateTimeFormat(pattern="dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	@NotNull(message="A data de nascimento da pessoa deve ser informada.")
	@Column(name="pessoa_data_nascimento", nullable=false, columnDefinition="DATE")
	public Date getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	
	@Column(name="pessoa_foto", nullable=true)
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	
	@Column(name="pessoa_content_type", nullable=true, length=100)
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	
	@OneToMany(mappedBy="pessoa_id")
	public List<Telefone> getListaTel(){
		return listaTel;
	}

	public void setListaTel(List<Telefone> listaTel) {
		this.listaTel = listaTel;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pessoa other = (Pessoa) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "Pessoa [id=" + id + ", nome=" + nome + ", endereco=" + endereco + ", ativo=" + ativo + ","
				+ "dataNascimento=" + dataNascimento + ", foto=" + foto + ", contentType="
				+ contentType + "]";
	}
	
}
