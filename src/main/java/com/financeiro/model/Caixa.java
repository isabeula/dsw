package com.financeiro.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

@Entity
@Table(name="TAB_CAIXA")
public class Caixa implements Serializable {

	private static final long serialVersionUID = 5737698097167590600L;

	private Integer id;
	private String caixaDescricao;
	private Date caixaData;
	private BigDecimal caixaValor;
	private TipoTransacao transacao_id;	
	
	public Caixa() {
		
	}
	
	public Caixa(Integer id, String caixa_descricao, Date caixa_data, BigDecimal caixa_valor, TipoTransacao tipo_transacao_id) {
		this.id = id;
		this.caixaDescricao = caixa_descricao;
		this.caixaData = caixa_data;
		this.caixaValor = caixa_valor;
		this.transacao_id = tipo_transacao_id;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="caixa_id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@NotBlank(message="Informe a descricao.") 
	@NotNull(message="Informe a descricao.")
	@Size(min=3, max=100, message="O tipo deve ter entre {min} e {max} caracteres. ")
	@Column(name="caixa_descricao",length=100, nullable=false)
	public String getCaixaDescricao() {
		return caixaDescricao;
	}
	public void setCaixaDescricao(String caixa_descricao) {
		this.caixaDescricao = caixa_descricao;
	}

	@DateTimeFormat(pattern="dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	@NotNull(message="A data deve ser informada.")
	@Column(name="caixa_data", nullable=false, columnDefinition="DATE")
	public Date getCaixaData() {
		return caixaData;
	}
	public void setCaixaData(Date data) {
		this.caixaData = data;
	}
	
	@NumberFormat(style=Style.CURRENCY, pattern="#,##0.00")
	@Column(name="caixa_valor", nullable=true, columnDefinition="DECIMAL(10,2) DEFAULT 0.00")
	public BigDecimal getCaixaValor() {
		return caixaValor;
	}
	public void setCaixaValor(BigDecimal caixaValor) {
		this.caixaValor = caixaValor;
	}
	
	@ManyToOne
	@JoinColumn(name="transacao_id")
	public TipoTransacao getTransacao_id() {
		return transacao_id;
	}
	public void setTransacao_id(TipoTransacao tipo) {
		this.transacao_id = tipo;
	}

	@Override
	public String toString() {
		return "Caixa [id=" + id + ", caixaDescricao=" + caixaDescricao + ", caixaData=" + caixaData + ", caixaValor="
				+ caixaValor + ", transacao_id=" + transacao_id + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Caixa other = (Caixa) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	
	
	
}
