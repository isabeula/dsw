package com.financeiro.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Embeddable
public class Endereco {
	
	private String logradouro;
	private String numero;
	private String complemento;
	private String bairro;
	private String cep;
	private String cidade;
	private String estado;
	
    @NotBlank(message="Informe o nome do logradouro. ") 
	@NotNull(message="Informe o nome do logradouro. ")
	@Size(min=2, max=100, message="O nome do logradouro deve ter entre {min} e {max} caracteres. ") 
	@Column(name="endereco_logradouro", length=100, nullable=false)
	public String getLogradouro() {
		return logradouro;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	
	@NotBlank(message="Informe o número do logradouro. ") 
	@NotNull(message="Informe o número do logradouro. ")
	@Size(min=1, max=5, message="O número do logradouro deve ter entre {min} e {max} caracteres. ") 
	@Column(name="endereco_numero", length=5, nullable=false)
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	@Size(min=2, max=50, message="O complemento do logradouro deve ter entre {min} e {max} caracteres. ") 
	@Column(name="endereco_complemento",length=50, nullable=true)
	public String getComplemento() {
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	
	
	@NotBlank(message="Informe o nome do bairro. ") 
	@NotNull(message="Informe o nome do bairro. ")
	@Size(min=2, max=50, message="O nome do bairro deve ter entre {min} e {max} caracteres. ") 
	@Column(name="endereco_bairro", length=50, nullable=false)
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	
	@NotBlank(message="Informe o cep. ") 
	@NotNull(message="Informe o cep. ")
	@Size(min=9, max=10, message="O cep deve ter entre {min} e {max} caracteres. ") 
	@Column(name="endereco_cep", length=10, nullable=false)
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	
	@NotBlank(message="Informe o nome da cidade. ") 
	@NotNull(message="Informe o nome da cidade. ")
	@Size(min=2, max=50, message="O nome da cidade deve ter entre {min} e {max} caracteres. ") 
	@Column(name="endereco_cidade", length=50, nullable=false)
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	
	@NotBlank(message="Informe o nome do estado. ") 
	@NotNull(message="Informe o nome do estado. ")
	@Size(min=2, max=2, message="O nome do estado deve ter entre {min} e {max} caracteres. ") 
	@Column(name="endereco_estado", length=2, nullable=false)
	public String getEstado() {
		return estado;
	}
	
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	
	

}
