package com.financeiro.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

@Entity
@Table(name="TAB_TIPO_TRANSACAO")
public class TipoTransacao implements Serializable {

	private static final long serialVersionUID = 1391651266613023535L;
	
	private Integer id;
	private String descricao;
	private List<Caixa> listaCaixa;
	
	public TipoTransacao() {
		
	}


	public TipoTransacao(Integer id, String descricao) {

		this.id = id;
		this.descricao = descricao;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="transacao_id")
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@NotBlank(message="Informe o nome da transação. ") 
	@NotNull(message="Informe o nome da transação.")
	@Size(min=3, max=100, message="O nome da transação deve ter entre {min} e {max} caracteres. ")
	@Column(name="transacao_descricao",length=100, nullable=false)
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
		
	@OneToMany(mappedBy="transacao_id")
	public List<Caixa> getListaCaixa(){
		return listaCaixa;
	}

	public void setListaCaixa(List<Caixa> listaCaixa) {
		this.listaCaixa = listaCaixa;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((listaCaixa == null) ? 0 : listaCaixa.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoTransacao other = (TipoTransacao) obj;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (listaCaixa == null) {
			if (other.listaCaixa != null)
				return false;
		} else if (!listaCaixa.equals(other.listaCaixa))
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "TipoTransacao [id=" + id + ", descricao=" + descricao + "]";
	}


}
