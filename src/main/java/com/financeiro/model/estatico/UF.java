package com.financeiro.model.estatico;

public enum UF {
	AC("AC","Acre"),
	AL("AL", "Alagoas"),
	DF("DF", "Distrito Federal"),
	SP("SP", "São Paulo"),
	RJ("RJ", "Rio de Janeiro"),
	MG("MG", "Minas Gerais");
	
	private String sigla;
	private String nomeEstado;
	
	private UF(String sigla, String nomeEstado) {
		this.sigla = sigla;
		this.nomeEstado = nomeEstado;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getNomeEstado() {
		return nomeEstado;
	}

	public void setNomeEstado(String nomeEstado) {
		this.nomeEstado = nomeEstado;
	}
	
	
}
