package com.financeiro.model.estatico;

public enum Modelo {
	Fixo("Tel","Fixo"),
	Celular("Cel","Celular"),
	Fax("Fax","Fax");
	
	private String tipo;
	private String nomeTipo;
	
	private Modelo(String tipo, String nomeTipo) {
		this.tipo = tipo;
		this.nomeTipo = nomeTipo;
	}

	public String getTipo() {
		return tipo;
	}

	public String getNomeTipo() {
		return nomeTipo;
	}
	
	
}
