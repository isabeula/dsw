package com.financeiro.model.estatico;

public enum Tipo {
	Residencial("Res","Residencial"),
	Comercial("Com","Comercial"),
	Recado("Rec","Recado"),
	Pessoal("Pes","Pessoal");
	
	private String siglaContato;
	private String nomeContato;
	
	private Tipo(String sigaContato, String nomeContato) {
		this.siglaContato = sigaContato;
		this.nomeContato = nomeContato;
	}

	public String getSiglaContato() {
		return siglaContato;
	}

	public String getNomeContato() {
		return nomeContato;
	}		
}
