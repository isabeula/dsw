package com.financeiro.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="TAB_TELEFONE")
public class Telefone implements Serializable {

	private static final long serialVersionUID = 5737698097167590600L;

	private Integer id;
	private String tipoTelefone;
	private String modeloTelefone;
	private String numeroTelefone;
	private Pessoa pessoa_id;	
	
	public Telefone() {
		
	}
	
	public Telefone(Integer id, String tipoTelefone, String modeloTelefone, String numeroTelefone,
			Pessoa pessoa_id) {
		this.id = id;
		this.tipoTelefone = tipoTelefone;
		this.modeloTelefone = modeloTelefone;
		this.numeroTelefone = numeroTelefone;
		this.pessoa_id = pessoa_id;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="telefone_id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@NotBlank(message="Informe o tipo do telefone.") 
	@NotNull(message="Informe o tipo do telefone.")
	@Size(min=3, max=100, message="O tipo deve ter entre {min} e {max} caracteres. ")
	@Column(name="telefone_tipo",length=100, nullable=false)
	public String getTipoTelefone() {
		return tipoTelefone;
	}
	public void setTipoTelefone(String tipoTelefone) {
		this.tipoTelefone = tipoTelefone;
	}
	
	@Column(name="telefone_modelo",length=100, nullable=false)
	public String getModeloTelefone() {
		return modeloTelefone;
	}
	public void setModeloTelefone(String modeloTelefone) {
		this.modeloTelefone = modeloTelefone;
	}
	
	@Column(name="telefone_numero",length=100, nullable=false)
	public String getNumeroTelefone() {
		return numeroTelefone;
	}
	public void setNumeroTelefone(String numeroTelefone) {
		this.numeroTelefone = numeroTelefone;
	}
	
	@ManyToOne
	@JoinColumn(name="pessoa_id")
	public Pessoa getPessoa_id() {
		return pessoa_id;
	}
	public void setPessoa_id(Pessoa pessoaId) {
		this.pessoa_id = pessoaId;
	}

	@Override
	public String toString() {
		return "Telefone [id=" + id + ", tipoTelefone=" + tipoTelefone + ", modeloTelefone=" + modeloTelefone
				+ ", numeroTelefone=" + numeroTelefone + ", pessoa_id=" + pessoa_id + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Telefone other = (Telefone) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}	
	
}
