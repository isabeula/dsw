package com.financeiro.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="TAB_PERMISSAO")
public class Permissao implements Serializable {

	private static final long serialVersionUID = 7430689148213048688L;
	
    private int id;
    private String nome;
    private List<RolePermissao> rolesPermissoes;
    
    
	public Permissao() {
		super();
	}
	public Permissao(int id, String nome) {
		super();
		this.id = id;
		this.nome = nome;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="permissao_id")
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@OneToMany(mappedBy="permissaoId")
	public List<RolePermissao> getRolesPermissoes() {
		return rolesPermissoes;
	}
	public void setRolesPermissoes(List<RolePermissao> rolesPermissoes) {
		this.rolesPermissoes = rolesPermissoes;
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Permissao other = (Permissao) obj;
		if (id != other.id)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Permissao [id=" + id + ", nome=" + nome + "]";
	}
    
    
}
