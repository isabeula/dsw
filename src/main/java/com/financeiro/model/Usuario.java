package com.financeiro.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

@Entity
@Table(name="TAB_USUARIO")
public class Usuario implements UserDetails, Serializable {


	private static final long serialVersionUID = 5737698097167590600L;
	
	private int id;
	private String email;
	private String username;
	private String password;
	private String contraSenha;
	//private Date lastLogin;
	private boolean ativo = true;
	
	private List<Role> roles;
	
		
	public Usuario() {
		super();
	}
	
	public Usuario(int id, String email, String password, String contraSenha,  boolean ativo) {
		super();
		this.id = id;
		this.email = email;
		this.password = password;
		this.contraSenha = contraSenha;
		//this.lastLogin = lastLogin;
		this.ativo = ativo;
	}
	
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="usuario_id")
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@Column(name="usuario_email")
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(name="usuario_psw")
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Transient
	public String getContraSenha() {
		return contraSenha;
	}
	public void setContraSenha(String contraSenha) {
		this.contraSenha = contraSenha;
	}
	
	/*@DateTimeFormat(pattern="dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	@Column(name="usuario_last_login",nullable=true, columnDefinition="DATE")
	public Date getLastLogin() {
		return lastLogin;
	}
	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}*/
	@NotNull
	@Column(name="usuario_ativo", nullable=false)
	public boolean isAtivo() {
		return ativo;
	}
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	
	
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name="TAB_USUARIO_ROLE", 
			   joinColumns=@JoinColumn(name="usuario_id"),
			   inverseJoinColumns = @JoinColumn(name="role_id")
			)
	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (id != other.id)
			return false;
		return true;
	}


	@Override
	@Transient
	public Collection<? extends GrantedAuthority> getAuthorities() {
		
		List<GrantedAuthority> autoridade = new ArrayList<GrantedAuthority>();
		for(Role role:this.getRoles()) {
			autoridade.add(new SimpleGrantedAuthority("ROLE_"+role.getNome().toUpperCase()));
		}
		return autoridade;
	}

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return this.username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	@Transient
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return ativo;
	}

	@Override
	@Transient
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return ativo;
	}

	@Override
	@Transient
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return ativo;
	}

	@Override
	@Transient
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return ativo;
	}	
	

}
